# Estrella

Los dominios solo permiten caracteres ASCII, para poder utilizar todo el rango
unicode, existe punycode (RFC 3492). En python se puede utilizar el método
`encode("punycode")` de la siguiente forma para ver como se codifica unicode
a ascii con punycode:

```py
>>> "árbol".encode("punycode")
b'rbol-4na'
>>> "💩".encode("punycode")
b'ls8h'
```

*Vemos que 💩 se traduce a ls8h*

Fijarse que se usa el `-` para la codificación, eso podría traer problemas en
las urls así que se definió IDNA (Internazionalization Domain Name Applications)
que agrega un prefijo `xn--` a las urls que vayan a usar punycode más algunas
pequeñas diferencias.

```py
>>> "💩.la".encode("idna")
b'xn--ls8h.la'
```

Hay un proof of concept de una forma de usar esto para hacer phishing:
https://www.xudongz.com/blog/2017/idn-phishing/

Básicamente hay un caracter de otro idioma con otro código unicode, que es
gráficamente igual a una `a`, esto se puede aprovechar para construir un
dominio que se ve igual a otro.

Ejemplo: Abrir esta url en firefox https://www.xn--80ak6aa92e.com/ nos lleva a
una página con una url indistinguible de la de apple, con https y todo.
(*chromium solucionó este problema, firefox considera que no es problema de los
browsers*)

# Temas con PEP8

- `pep8` como comando está deprecado (y desactualizado) en favor de
  `pycodestyle`
- `pep8` señala `hget.py:167:13: W503 line break before binary operator`
  pero esto [ya no es un warning](https://hg.python.org/peps/rev/3857909d7956)
- `pycodestyle` por otro lado introduce los warnings:

      hget.py:126:5: E722 do not use bare 'except'
      hget.py:132:9: E722 do not use bare 'except'

  ya que es mala práctica usar except a secas por que captura cosas como Ctrl-C,
  terminación externa del programa, etc.
